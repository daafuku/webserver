Replacing nginx with main.go
===

Features required:
---

- TLS support
- non-TLS request redirects
- proxy passing from subdomains and sudirectories to various services running on the host machine.
- hosting static html pages
- websocket protocol upgrading passthrough