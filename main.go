package main

import (
	"bufio"
	"bytes"
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"github.com/pelletier/go-toml/v2"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

type service struct {
	Name    string
	Addr    string
	Path    string
	UseAuth bool
}

type config struct {
	Addr            string
	InsecureAddr    string
	BaseURL         string
	Username        string
	PasswordHash    string
	CookieStoreName string
	Services        []service
	TLSKey          string
	TLSCert         string
	StaticRoot      string
}

var (
	cfg    config
	logger *logrus.Logger
)

type defaultLoggerLogrusWrapper struct {
	logger logrus.FieldLogger
}

func (l *defaultLoggerLogrusWrapper) Write(b []byte) (n int, err error) {
	l.logger.Error(string(b))
	return len(b), nil
}

func initLogging() {
	logger = logrus.New()
	logger.Out = os.Stderr
	logger.SetFormatter(&logrus.TextFormatter{})

	log.Default().SetFlags(0)
	log.Default().SetOutput(&defaultLoggerLogrusWrapper{
		logger: logger.WithField("wrapped", true),
	})
}

func proxyPass(host string, path string) http.Handler {

	director := func(req *http.Request) {
		req.URL.Scheme = "http"
		req.URL.Host = host
		req.URL.Path = "/" + strings.TrimPrefix(req.URL.Path, path)
	}

	return &httputil.ReverseProxy{
		Director: director,
	}
}

func healthCheck(rw http.ResponseWriter, r *http.Request) {
	s := &bytes.Buffer{}
	healthy := true

	for _, service := range cfg.Services {
		u := &url.URL{
			Host:   service.Addr,
			Scheme: "http",
		}

		res, err := http.Get(u.String())

		s.WriteString(service.Name)
		s.WriteString(" -> ")

		if err != nil {
			s.WriteString(err.Error())
			healthy = false
		} else {
			s.WriteString(http.StatusText(res.StatusCode))
			if res.StatusCode/100 != 2 {
				healthy = false
			}
		}

		s.WriteRune('\n')
	}

	if healthy {
		s.WriteString("healthy")
	} else {
		s.WriteString("sick")
	}

	rw.Header().Set("Content-Type", "text/plain")
	rw.Header().Set("Content-Length", strconv.Itoa(s.Len()))
	rw.WriteHeader(http.StatusOK)
	rw.Write(s.Bytes())
}

var cookieStore = sessions.NewCookieStore(securecookie.GenerateRandomKey(32))

func validate(username, password string) error {
	if username == cfg.Username {
		if err := bcrypt.CompareHashAndPassword([]byte(cfg.PasswordHash), []byte(password)); err != nil {
			logger.Println(err)
			return fmt.Errorf("password is incorrect")
		}
		return nil
	}
	return fmt.Errorf("username is incorrect")
}

func authenticate(rw http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		username := r.FormValue("username")
		password := r.FormValue("password")

		if err := validate(username, password); err != nil {
			http.Error(rw, err.Error(), http.StatusUnauthorized)
			return
		}

		session, err := cookieStore.New(r, cfg.CookieStoreName)
		if err != nil {
			logger.Println(fmt.Errorf("failed to load session: %w", err))

			if session == nil {
				logger.Println(fmt.Errorf("failed to create session: %w", err))
				goto bail
			}
		}

		session.Options.SameSite = http.SameSiteStrictMode
		session.Options.Secure = true

		session.Values["username"] = username

		err = cookieStore.Save(r, rw, session)
		if err != nil {
			logger.Println(fmt.Errorf("failed to save session: %w", err))
			goto bail
		}

		http.Error(rw, "authenticated", http.StatusOK)
		return

	bail:
		http.Error(rw, "failed to authenticate", http.StatusInternalServerError)
		return
	} else if r.Method == http.MethodGet {
		htmlString := `<html><head><title>DarfkNet Login</title></head><body>
<form action="/authenticate" method="POST">
	<input type="text" name="username" placeholder="username"/>
	<input type="password" name="password" placeholder="password"/>
	<button type="submit">login</button>
</form>
</body></html>`

		rw.Header().Add("Content-Type", "text/html; charset=utf-8")
		rw.Write([]byte(htmlString))
		return
	}
}

type loggingResponseWriter struct {
	http.ResponseWriter
	code    int
	bodyLen int
}

func (w *loggingResponseWriter) WriteHeader(code int) {
	w.code = code
	w.ResponseWriter.WriteHeader(code)
}

func (w *loggingResponseWriter) Write(body []byte) (int, error) {
	w.bodyLen = len(body)
	return w.ResponseWriter.Write(body)
}

func (w *loggingResponseWriter) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	h, ok := w.ResponseWriter.(http.Hijacker)
	if !ok {
		return nil, nil, fmt.Errorf("base ResponseWriter is not a hijacker")
	}

	return h.Hijack()
}

type requestContext struct {
	username      string
	authenticated bool
}

type requestContextKey struct{}

func initRequestContextMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		key := requestContextKey{}
		value := &requestContext{}
		r = r.Clone(context.WithValue(r.Context(), key, value))
		next.ServeHTTP(rw, r)
	})
}

func getRequestContext(r *http.Request) *requestContext {
	return r.Context().Value(requestContextKey{}).(*requestContext)
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		c := getRequestContext(r)

		lrw := loggingResponseWriter{
			ResponseWriter: rw,
		}

		started := time.Now()
		next.ServeHTTP(&lrw, r)
		elapsed := time.Since(started)

		entry := logger.WithFields(logrus.Fields{
			"remote":          r.RemoteAddr,
			"code":            lrw.code,
			"bodyLen":         lrw.bodyLen,
			"path":            r.URL.Path,
			"duration":        elapsed.String(),
			"isAuthenticated": c.authenticated,
		})

		if c.authenticated {
			entry = entry.WithFields(logrus.Fields{
				"username": c.username,
			})
		}

		entry.Info("request")
	})
}

// authenticationMiddleware is responsible for loading the request context with the user information present in the cookie
func authenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		if session, err := cookieStore.Get(r, cfg.CookieStoreName); err == nil && session != nil && !session.IsNew {
			reqCtx := getRequestContext(r)

			if username, exists := session.Values["username"]; exists {
				if username, ok := username.(string); ok {
					reqCtx.username = username
					reqCtx.authenticated = true
				}
			}

		} else {
			logger.Println(err, session, session.IsNew)
		}

		next.ServeHTTP(rw, r)
	})
}

// authorizationMiddleware is responsible for rejecting the request if the user is not authenticated
func authorizationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		c := getRequestContext(r)
		if c.authenticated {
			next.ServeHTTP(rw, r)
			return
		}
		http.Error(rw, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
	})
}

// makeIsUserMiddleware returns middleware that rejects the request if the username present in
// the request context does not match username
func makeIsUserMiddleware(username string) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			c := getRequestContext(r)
			if c.authenticated && c.username == username {
				next.ServeHTTP(rw, r)
				return
			}
			http.Error(rw, http.StatusText(http.StatusForbidden), http.StatusForbidden)
		})
	}
}

func main() {
	background, cancel := context.WithCancel(context.Background())

	var configPath string
	flag.StringVar(&configPath, "c", "/etc/webserver/config.toml", "configuration file")
	flag.Parse()

	initLogging()

	if fd, err := os.Open(configPath); err != nil {
		logger.WithError(err).WithField("file", configPath).Error("failed to load config file")
		return
	} else {
		defer fd.Close()
		if err := toml.NewDecoder(fd).Decode(&cfg); err != nil {
			logger.WithError(err).WithField("file", configPath).Error("failed to decode config file")
			return
		}
		logger.Println("loaded config file")
	}

	var baseURL *url.URL

	{
		sane := true
		for _, service := range cfg.Services {
			if !strings.HasSuffix(service.Path, "/") {
				logger.WithField("service", service.Name).Error("service path must end in /")
				sane = false
			}
		}

		{
			var err error
			baseURL, err = url.Parse(cfg.BaseURL)
			if err != nil {
				logger.WithError(err).WithField("redirectURL", cfg.BaseURL).Error("failed to parse redirect url")
				sane = false
			}
		}

		if !sane {
			logger.Fatal("insane config file")
		}
	}

	root := mux.NewRouter()
	root.Use(initRequestContextMiddleware)
	root.Use(loggingMiddleware)
	root.Use(authenticationMiddleware)

	router := root.NewRoute().Subrouter()

	authenticated := router.NewRoute().Subrouter()
	authenticated.Use(authorizationMiddleware)
	authenticated.Use(makeIsUserMiddleware("tom"))

	for _, service := range cfg.Services {
		var router *mux.Router = router
		service := service

		// add the unauthenticated redirects in
		router.Path(strings.TrimSuffix(service.Path, "/")).
			HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
				u := *baseURL
				u.Path = service.Path
				http.Redirect(rw, r, u.String(), http.StatusPermanentRedirect)
			})

		if service.UseAuth {
			router = authenticated
		}

		router.PathPrefix(service.Path).
			Handler(proxyPass(service.Addr, service.Path))
	}

	authenticated.Path("/health").HandlerFunc(healthCheck)

	router.Path("/authenticate").HandlerFunc(authenticate)

	router.PathPrefix("/").Handler(http.FileServer(http.Dir(cfg.StaticRoot)))

	insecureRedirectRouter := mux.NewRouter()
	insecureRedirectRouter.PathPrefix("/").
		HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			u := *baseURL
			u.Path = r.URL.Path
			http.Redirect(rw, r, u.String(), http.StatusPermanentRedirect)
		})

	server := &http.Server{
		Addr:    cfg.Addr,
		Handler: root,
	}

	insecureServer := &http.Server{
		Addr:    cfg.InsecureAddr,
		Handler: insecureRedirectRouter,
	}

	// this goroutine captures interrupt signals, after which it cancels the
	// application's background context
	go func() {
		ch := make(chan os.Signal, 4)
		signal.Notify(ch, os.Interrupt)

		select {
		case <-background.Done():
		case sig := <-ch:
			logger.WithField("signal", sig.String()).Info("caught signal, exiting")
		}
		cancel()
	}()

	go func() {
		err := server.ListenAndServeTLS(cfg.TLSCert, cfg.TLSKey)
		if err != nil {
			logger.WithError(err).Info("server closed")
		}
		cancel()
	}()

	go func() {
		err := insecureServer.ListenAndServe()
		if err != nil {
			logger.WithError(err).Info("insecure server closed")
		}
		cancel()
	}()

	<-background.Done()
	logger.WithError(background.Err()).Info("app finished")
	cancel()

	shutdown, shutdownCancel := context.WithTimeout(context.Background(), time.Minute)
	err := server.Shutdown(shutdown)
	logger.WithError(err).Info("app shutdown")
	shutdownCancel()
}
