module gitlab.com/daafuku/webserver

go 1.17

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.2.1
	github.com/pelletier/go-toml/v2 v2.0.0-beta.5
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
)

require golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
